const express = require('express')
const jwt = require('jsonwebtoken')
const crypto = require('crypto')


const mailer = require('../../modules/mailer')
const authConfig = require('../../config/auth.json')
const Feed = require('../models/feed')
const router = express.Router();


function generatorToken(params = {}){

    return jwt.sign(params, authConfig.secret, {
        expiresIn: 86400,
    } )
}

router.get('/', async (req, res) => {
    try{
        const feed = await Feed.find().populate(
            ['feed']);

        return res.send({ feed,
        token: generatorToken( { id: feed.id, name: feed.name }) 
        });
    } catch(err) {
        return res.status(400).send({ error: 'Error loading feed' });
    }
});

router.post('/register', async(req, res) => {
    try{
        const feed = await Feed.create(req.body)

        return res.send( feed )
    }catch(err){
        return res.status(400).send({error: 'Registration failed'})
    }
})


router.post('/authenticate', async (req, res) => {
  //res.send('oigg')

    const {email, password } = req.body;

    const feed = await Feed.findOne({ email }).select('+password');

    if(!feed)
        return res.status(400).send({error: 'Feed not found'})

    // console.log(password)
    // console.log( feed.password)

    if(await password !== feed.password )
        return res.status(400).send({error: 'Invalid Password'});

             feed.password = undefined;


        //res.send('Usuário logado')



        res.send( { feed,
                token: generatorToken( {id: feed.id }) })
        //res.send( feed )
})


router.post('/forgot_password', async(req, res) => {
    const { email } = req.body;

    try{
        const feed = await Feed.findOne( { email } )
        if(!feed)
            return res.status(400).send({error: 'Feed not found' });

        const token = crypto.randomBytes(20).toString('hex');

        const now = new Date();
        now.setHours(now.getHours() + 1);

        await Feed.findByIdAndUpdate(feed.id, {
            '$set':{
                passwordResetToken : token,
                passwordResetExpires: now,
            }
        })
        //console.log(token, email)
        mailer.sendMail({
            to:  email ,
            from: 'alvarodeviveirosneto@gmail.com',
            template: '/src/app/resources/mail/auth/forgot_password',
            //context: { token },
            html: '<p>Você esqueceu sua senha? Não tem problema, utilize esse token: ' +  token + '</p>'
        },(err)=>{
            if(err)
                return res.status(400).send({error: 'Cannot send forgot password email' });


            return res.send('Email enviado com sucesso. Seu Token é: '+ token );
        })

       // console.log("errosss: " + token, now)

    }catch(err){
        res.status(400).send({error: 'Erro on forgot password, try again'})
        // console.log(err)
    }

})

router.post('/reset_password', async(req, res) => {
    const {email, token, password } = req.body;
    //console.log(email)
    try{
        const feed = await Feed.findOne( { email } )
            .select('+passwordResetToken passwordResetExpires')

        if(!feed)
          return res.status(400).send({error: 'Feed not found'})


        if(token !== feed.passwordResetToken)
            return res.status(400).send({error: 'Token invalid'})
        const now = new Date();

        if(now > feed.passwordResetExpires)
            return res.status(400).send({error: 'Token expired, generate a new one'})

        feed.password = password;

        await feed.save();
        res.send();


    }catch(err) {
        res.status(400).send({error: 'Cannot reset password, try again'});
    }
})

router.delete('/:feedId', async (req, res) => {
    try{
        const feed = await Feed.findByIdAndRemove(req.params.feedId);

        return res.send(feed + " - Deletada");
    } catch(err) {
        return res.status(400).send({ error: 'Error deleting feed' });
    }
});





module.exports = app => app.use('/feed', router);