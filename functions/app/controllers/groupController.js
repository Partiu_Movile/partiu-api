const express = require ('express')
const authMiddleware = require('../middlewares/auth');
const router = express.Router();

const Group = require('../models/groups');
const Member = require('../models/members');
const List = require('../models/list');

router.use(authMiddleware);

router.get('/', async (req, res) => {
    try{
        const groups = await Group.find().populate(
            ['user', 
                ['members', {
                    path: 'members',
                    model: 'User'
          } 
        ]]);

        return res.send({ groups });
    } catch(err) {
        return res.status(400).send({ error: 'Error loading groups' });
    }
});

router.get('/:groupId', async (req, res) => {
    try{
        const group = await Group.findById(req.params.groupId).populate(['user', 'member']);

        return res.send({ group });
    } catch(err) {
        return res.status(400).send({ error: 'Error loading group' });
    }
});

router.post('/', async (req, res) => {
    try{
        const { title, type, members } =  req.body;

        const group = await Group.create({ title, type, user: req.userId });

        await Promise.all(members.map(async member => {
            const groupMember = new Member({ ...member, group: group._id });

            await groupMember.save();

            group.members.push(groupMember);
        }));

        await group.save();

        return res.send({ group });
    } catch(err) {
        return res.status(400).send({ error: 'Error creating new group' });
    }
});

router.put('/:groupId', async (req, res) => {
    try{
        const {members } =  req.body;

        const group = await Group.findByIdAndUpdate(req.params.groupId);

            group.members = [];
            await Member.remove({ group: group._id });

        await Promise.all(members.map( async member => {
            const groupMember = new Member({ ...member, group: group._id });

            await groupMember.save();

            group.members.push(groupMember);
        }));

        await group.save();

        return res.send({ group });
    } catch(err) {
        return res.status(400).send({ error: 'Error updating group' });
    }
});

router.delete('/:groupId', async (req, res) => {
    try{
        const group = await Group.findByIdAndRemove(req.params.groupId);

        return res.send();
    } catch(err) {
        return res.status(400).send({ error: 'Error deleting group' });
    }
});

module.exports = app => app.use('/groups', router)