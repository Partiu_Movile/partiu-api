const db = require('../../database/firestore')

const express = require('express')
const jwt = require('jsonwebtoken')
const crypto = require('crypto')

const mailer = require('../../modules/mailer')
const authConfig = require('../../config/auth.json')
const Company = require('../models/company')
const router = express.Router();


function generatorToken(params = {}){

    return jwt.sign(params, authConfig.secret, {
        expiresIn: 86400,
    } )
}

router.get('/', async (req, res) => {
    try{
        const company = await Company.find().populate(
            ['company']);

        return res.send({ company,
        token: generatorToken( { id: company.id }) 
        });
    } catch(err) {
        return res.status(400).send({ error: 'Error loading Empresas' });
    }
});


router.post('/register', async(req, res) => {
    try{
        const{ email, password } = req.body;
        if(await Company.findOne({ email }))
            return res.status(400).send({error: 'Company already exists'})

        if(password = null)
            return res.status(400).send({error: 'Password can not be null'})

        const comp = await Company.create(req.body)

        comp.password = undefined;

        return res.send( { comp,
            token: generatorToken( { id: comp.id })
        })
    }catch(err){
        return res.status(400).send({error: 'Registration failed'})
    }
})


router.post('/authenticate', async (req, res) => {
  //res.send('oigg')

    const {email, password } = req.body;

    const comp = await Company.findOne({ email }).select('+password');

    if(!comp)
        return res.status(400).send({error: 'Company not found'})

    // console.log(password)
    // console.log( comp.password)

    if(await password !== comp.password )
        return res.status(400).send({error: 'Invalid Password'});

             comp.password = undefined;


        //res.send('Usuário logado')



        res.send( { comp,
                token: generatorToken( {id: comp.id }) })
        //res.send( comp )
})


router.post('/forgot_password', async(req, res) => {
    const { email } = req.body;

    try{
        const comp = await Company.findOne( { email } )
        if(!comp)
            return res.status(400).send({error: 'Company not found' });

        const token = crypto.randomBytes(20).toString('hex');

        const now = new Date();
        now.setHours(now.getHours() + 1);

        await Company.findByIdAndUpdate(comp.id, {
            '$set':{
                passwordResetToken : token,
                passwordResetExpires: now,
            }
        })
        //console.log(token, email)
        mailer.sendMail({
            to:  email ,
            from: 'alvarodeviveirosneto@gmail.com',
            template: '/src/app/resources/mail/auth/forgot_password',
            //context: { token },
            html: '<p>Você esqueceu sua senha? Não tem problema, utilize esse token: ' +  token + '</p>'
        },(err)=>{
            if(err)
                return res.status(400).send({error: 'Cannot send forgot password email' });


            return res.send('Email enviado com sucesso. Seu Token é: '+ token );
        })

       // console.log("errosss: " + token, now)

    }catch(err){
        res.status(400).send({error: 'Erro on forgot password, try again'})
        // console.log(err)
    }

})

router.post('/reset_password', async(req, res) => {
    const {email, token, password } = req.body;
    //console.log(email)
    try{
        const comp = await Company.findOne( { email } )
            .select('+passwordResetToken passwordResetExpires')

        if(!comp)
          return res.status(400).send({error: 'Company not found'})


        if(token !== comp.passwordResetToken)
            return res.status(400).send({error: 'Token invalid'})
        const now = new Date();

        if(now > comp.passwordResetExpires)
            return res.status(400).send({error: 'Token expired, generate a new one'})

        comp.password = password;

        await comp.save();
        res.send();


    }catch(err) {
        res.status(400).send({error: 'Cannot reset password, try again'});
    }
})

router.delete('/:companyId', async (req, res) => {
    try{
        const company = await Company.findByIdAndRemove(req.params.companyId);

        return res.send(company + " - Deletada");
    } catch(err) {
        return res.status(400).send({ error: 'Error deleting company' });
    }
});






module.exports = app => app.use('/authCompany', router);