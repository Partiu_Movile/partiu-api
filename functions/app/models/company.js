const mongoose = require('../../database/mongo')
const bcrypt = require('bcryptjs')
const Schema = mongoose.Schema;

var CreateCompany = new Schema({

    name:{
        type:String,
        require:true,
    },
    email:{
        type:String,
        unique:true,
        require:true,
        lowercase:true,
    },
    password:{
        minlength:8,
        type:String,
        require:true,
        select:false,
    },
    passwordResetToken:{
        type: String,
        select: false
    },
    passwordResetExpires:{
        type:Date,
        select:false
    },
    createAt:{
        type:Date,
        default: Date.now
    },


})

const Company = mongoose.model('Company',CreateCompany)

module.exports = Company;

