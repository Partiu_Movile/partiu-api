const mongoose = require('../../database/mongo')
const bcrypt = require('bcryptjs')
const Schema = mongoose.Schema;

var CreateFeed = new Schema({

    name:{
        type:String,
        require:true,
    },
    wish:{
        type: Boolean,
        select: false
    },
    favorite:{
        type: Boolean,
        select: false
    },
    company:{
        type:String,
    },
    rate:{
        type: Number,
        default: 0
    },
    recommended:[{
        icon: String,
        title: String
    }],
    status: {
        type: Boolean
    },
    price: {
        type: Number
    },
    flux: {
        type: String
    },
    images: [{
        id: Number,
        image: String
    }],
    description: {
        type: String
    },
    operation:[{
        id: Number,
        description: String
    }],
    address:{
        type: String
    },
    location:{
        type: String
    },
    menu:[{
        id: Number,
        title: String,
        items:[{
            id: Number,
            name: String,
            short_description: String,
            description: String,
            price: String,
            old_price: String,
            image: String
        }],
    }],
    createAt:{
        type:Date,
        default: Date.now
    },
})

const Feed = mongoose.model('feed',CreateFeed)




module.exports = Feed;