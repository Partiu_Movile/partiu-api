const mongoose = require('../../database/mongo')
const bcrypt = require('bcryptjs')
const Schema = mongoose.Schema;

var CreateMember = new Schema({

    group: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Group',
        require: true,
    },
    member: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require: true,
    },
    lists: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'List',
    }],
    createAt:{
        type:Date,
        default: Date.now
    },
})

const Member = mongoose.model('Member',CreateMember)




module.exports = Member;

