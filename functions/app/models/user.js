const mongoose = require('../../database/mongo')
const bcrypt = require('bcryptjs')
const Schema = mongoose.Schema;

var CreateUser = new Schema({

    // name_phone: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'Fila',
    //     require: true,
    // },
    email:{
        type:String,
        unique:true,
        require:true,
        lowercase:true,
    },
    password:{
        type:String,
        require:true,
        select:false,
    },
    passwordResetToken:{
        type: String,
        select: false
    },
    passwordResetExpires:{
        type:Date,
        select:false
    },
    createAt:{
        type:Date,
        default: Date.now

    },


})
/*
UserSchema.pre('save', async  (next) => {
    const hash =  await bcrypt.hash(this.password, 2);
    this.password = hash;
    console.log(hash);
    next();

})
*/

const User = mongoose.model('User',CreateUser)




module.exports = User;

