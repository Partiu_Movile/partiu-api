const mongoose = require('../../database/mongo')
const bcrypt = require('bcryptjs')
const Schema = mongoose.Schema;

var CreateList = new Schema({

    group: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Group',
        require: true,
    },
    
    lists: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'List',
    }],
    completed: {
        type: Boolean,
        require: true,
        default: false,
    },
    createAt:{
        type:Date,
        default: Date.now

    },
})

const List = mongoose.model('List',CreateList)




module.exports = List;

