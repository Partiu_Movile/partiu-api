const mongoose = require('../../database/mongo')
const bcrypt = require('bcryptjs')
const Schema = mongoose.Schema;

var CreateGroup = new Schema({

    title:{
        type:String,
        require:true,
    },
    type: {
        type: Boolean,
        require: true,
        default: 0
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require: true,
    },
    lists: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'List',
    }],
    members: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Member',
    }],
    createAt:{
        type:Date,
        default: Date.now
    },
    comanda: {
        type: Boolean,
        require: true,
        default: false,
    },


})
/*
GroupSchema.pre('save', async  (next) => {
    const hash =  await bcrypt.hash(this.password, 2);
    this.password = hash;
    console.log(hash);
    next();

})
*/

const Group = mongoose.model('Group',CreateGroup)




module.exports = Group;

