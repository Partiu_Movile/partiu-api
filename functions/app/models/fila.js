const mongoose = require('../../database/mongo')
const Schema = mongoose.Schema;

var CreateFila = new Schema({

    name:{
        type:String,
        require:true,
    },
    phone:{
        type: Number,
        unique:true,
        require:true,
    },
    place:{
        type: Number,
        require: true,
    },
    priority:{
        type: Number,
        require: true,
    },
    createAt:{
        type:Date,
        default: Date.now
    },
})

// Priority
// 0 = low
// 1 = medium
// 2 = high

const Fila = mongoose.model('Fila',CreateFila)




module.exports = Fila;

