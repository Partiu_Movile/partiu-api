const functions = require('firebase-functions');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', (req, res) => {
    res.send("Partiu-api funfando ;)")
})

require('./app/controllers/index')(app)


app.listen(8000, (err) => {
    if(err) return console.log(err)
        console.log('Hora de codar! - Porta: 8000')
})